# One Click Raster Stacking

Adds a toolbar button that lets you quickly stack all raster bands with matching CRS into a single raster to allow RGB visualisations using bands from different rasters files.

Stacking raster bands is of course possible with in-build QGIS/GDAL functionality, but this One Click Solution does the job mutch faster. 

## Usage

1. Open all raster that you want to stack in QGIS.
2. Select one raster, which defines the target CRS.
3. Click the ![](icon.png) toolbar button to stack all raster with matching CRS.

The result stack is a lightweigt in-memory VRT raster and will be opened inside QGIS.
