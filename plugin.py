import datetime
from os.path import join, dirname, exists, basename

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QToolButton, QMenu, QCheckBox, QWidgetAction
from osgeo import gdal
from qgis.PyQt import QtCore
from qgis._core import QgsRasterLayer, QgsProject
from qgis._gui import QgisInterface

from oneclickrasterstacking.bandlist import BandList


class Plugin(object):

    def __init__(self, iface):
        assert isinstance(iface, QgisInterface)
        self.iface = iface
        self.icon = QIcon(join(dirname(__file__), 'icon.png'))

    def initGui(self):
        self.bandList = BandList(iface=self.iface, parent=self.iface.parent())
        self.toolButtonStacking = QToolButton()
        self.toolButtonStacking.setText('One Click Raster Stacking')
        self.toolButtonStacking.setToolTip('One Click Raster Stacking')
        self.toolButtonStacking.setIcon(self.icon)
        toolMenu = QMenu()
        self.checkBoxSubsetting = QCheckBox('show subsetting dialog', toolMenu)
        checkableAction = QWidgetAction(toolMenu)
        checkableAction.setDefaultWidget(self.checkBoxSubsetting)
        toolMenu.addAction(checkableAction)
        self.toolButtonStacking.setMenu(toolMenu)
        self.toolButtonStacking.setPopupMode(QToolButton.MenuButtonPopup)  # InstantPopup)
        self.iface.addToolBarWidget(self.toolButtonStacking)

        self.toolButtonStacking.clicked.connect(self.run)
        self.checkBoxSubsetting.toggled.connect(lambda checked: self.bandList.hide())

    def unload(self):
        pass
        # self.iface.removeToolBarIcon(self.action)
        # self.iface.removeDockWidget(self.ui)

    def run(self):
        selectedLayers = self.iface.layerTreeView().selectedLayers()

        if len(selectedLayers) != 1:
            self.iface.messageBar().pushCritical(
                'Quick Raster Stacking usage error', 'Select a single raster layer defining the CRS for stacking.'
            )
            return

        selectedLayer = selectedLayers[0]
        if not isinstance(selectedLayer, QgsRasterLayer):
            self.iface.messageBar().pushCritical(
                'Quick Raster Stacking usage error', 'Select a single raster layer defining the CRS for stacking.'
            )

        timestamp = str(datetime.datetime.now()).replace(' ', 'T').replace(':', '')
        filenames = list()
        bandNames = list()
        sources = list()
        i1 = 0
        for layer in QgsProject.instance().mapLayers().values():
            if not isinstance(layer, QgsRasterLayer):
                continue
            if not exists(layer.source()):
                continue
            if layer.crs() != selectedLayer.crs():
                continue
            i1 += 1

            ds: gdal.Dataset = gdal.Open(layer.source())
            for i2 in range(ds.RasterCount):
                rb: gdal.Band = ds.GetRasterBand(i2 + 1)
                filename = f"/vsimem/raster_{i1}_band_{i2 + 1}_{timestamp}.vrt"
                options = gdal.TranslateOptions(bandList=[i2 + 1], format='VRT')
                gdal.Translate(filename, ds, options=options)
                filenames.append(filename)
                bandname = rb.GetDescription()
                if bandname == '':
                    bandname = f'Band {i2 + 1}'
                bandNames.append(f'{layer.name()} - {bandname}')
                sources.append(layer.source())

        if len(filenames) == 0:
            self.iface.messageBar().pushCritical(
                'Quick Raster Stacking', 'No matching raster found.'
            )
            return

        # show and update band list
        self.bandList.TIMESTAMP = timestamp
        self.bandList.FILENAMES = filenames
        self.bandList.BANDNAMES = bandNames
        self.bandList.mBandList.clear()
        items = [bandName + ' @ ' + source for bandName, source in zip(bandNames, sources)]
        self.bandList.mBandList.addItems(items)
        self.bandList.mBandList.selectAll()
        self.bandList.mBandList.setFocus()
        self.bandList.mCreateStack.setEnabled(True)

        if self.checkBoxSubsetting.isChecked():
            self.bandList.show()
        else:
            self.bandList.hide()
            self.bandList.onCreateStackClicked()
