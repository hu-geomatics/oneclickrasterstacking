from qgis.PyQt.QtCore import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtGui import *
from qgis.gui import *
from qgis.core import *

from enmapbox.testing import initQgisApplication
from enmapbox import EnMAPBox
from oneclickrasterstacking import Plugin

try:
    import qgisresources.images
    qgisresources.images.qInitResources()
except:
    pass

# start application and open test dataset
qgsApp = initQgisApplication()
layer1 = QgsRasterLayer(r'C:\source\QGISPlugIns\enmap-box\enmapboxtestdata\enmap_berlin.bsq', baseName='enmap_berlin')
layer2 = QgsRasterLayer(r'C:\source\QGISPlugIns\enmap-box\enmapboxtestdata\hires_berlin.bsq', baseName='hires_berlin')
layers = [layer1, layer2]

QgsProject.instance().addMapLayers(layers)

class TestInterface(QgisInterface):

    def __init__(self):
        QgisInterface.__init__(self)

        self.ui = QMainWindow()
        self.ui.setWindowTitle('QGIS')
        self.ui.setWindowIcon(QIcon(r'C:\source\QGIS3-master\images\icons\qgis_icon.svg'))
        self.ui.resize(QSize(1500, 750))
        self.ui.show()

    def addDockWidget(self, area, dockwidget):
        self.ui.addDockWidget(area, dockwidget)


def test_plugin():
    from rasterdataplotting.rasterdataplotting.plugin import RdpPlugin

    iface = TestInterface()

    plugin = Plugin(iface=iface)
    plugin.initGui()
    qgsApp.exec_()

if __name__ == '__main__':
    test_plugin()
