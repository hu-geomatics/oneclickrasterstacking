[general]
name=One Click Raster Stacking
description=Adds a toolbar button that lets you quickly stack all raster bands with matching CRS into a single raster to allow RGB visualisations using bands from different rasters files. Stacking raster bands is of course possible with in-build QGIS/GDAL functionality, but this One Click Solution does the job mutch faster.
version=1.1.0
about=See the project homepage for usage instructions.
qgisMinimumVersion=3.0
qgisMaximumVersion=4.0
author=Andreas Rabe
email=andreas.rabe@geo.hu-berlin.de
tags=raster, band, stacking, multi-source, visualization
icon=icon.png
experimental=False
homepage=https://bitbucket.org/hu-geomatics/oneclickrasterstacking/wiki/Home
tracker=https://bitbucket.org/hu-geomatics/oneclickrasterstacking/issues
repository=https://bitbucket.org/hu-geomatics/oneclickrasterstacking
changelog=1.1.0
    - Added option for selecting a band subset before stacking.
    1.0.1
    - Fixed bug with tmp filenames.
    1.0
    - Initial upload.
