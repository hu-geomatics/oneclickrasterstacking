import datetime
from os.path import join, dirname, basename

from PyQt5 import uic
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QListWidget, QToolButton
from osgeo import gdal
from qgis._gui import QgisInterface, QgsMessageBar


class BandList(QMainWindow):
    mBandList: QListWidget
    mSelectAll: QToolButton
    mCreateStack: QToolButton

    def __init__(self, iface: QgisInterface, parent=None):
        QMainWindow.__init__(self, parent)
        uic.loadUi(join(dirname(__file__), 'bandlist.ui'), self)
        self.iface = iface

        self.TIMESTAMP = ''
        self.FILENAMES = list()
        self.BANDNAMES = list()

        self.icon = QIcon(join(dirname(__file__), 'icon.png'))
        self.mCreateStack.setIcon(self.icon)

        self.mCreateStack.clicked.connect(self.onCreateStackClicked)
        self.mSelectAll.clicked.connect(self.onSelectAllClicked)

    def onSelectAllClicked(self):
        self.mBandList.selectAll()
        self.mBandList.setFocus()

    def onCreateStackClicked(self):
        from oneclickrasterstacking import Plugin

        self.mCreateStack.setEnabled(False)

        # get selected bands
        indices = [x.row() for x in self.mBandList.selectedIndexes()]

        if len(indices) == 0:
            self.mCreateStack.setEnabled(True)
            return

        filenames = [self.FILENAMES[index] for index in indices]
        bandnames = [self.BANDNAMES[index] for index in indices]

        # build stack
        filename = f"/vsimem/stack_{len(filenames)}bands_{self.TIMESTAMP}.vrt"
        ds = gdal.BuildVRT(filename, filenames, options=gdal.BuildVRTOptions(separate=True, resolution='highest'))
        for i2, bandname in enumerate(bandnames):
            rb: gdal.Band = ds.GetRasterBand(i2 + 1)
            rb.SetDescription(bandname)
        ds.FlushCache()
        del ds

        self.iface.addRasterLayer(filename, basename(filename))
        self.mCreateStack.setEnabled(True)
        self.hide()
        self.iface.messageBar().pushSuccess('Quick Raster Stacking', f'Created in-memory VRT {basename(filename)}')
